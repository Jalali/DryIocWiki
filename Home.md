# Wiki Home

[TOC]

## Getting Started

Starting from simple interface/implementation setup:
```
#!c#
    public interface IService { }
    public class SomeService : IService { }
    
    public interface IClient { IService Service { get; } }
    public class SomeClient : IClient
    {
        public IService Service { get; private set; }
        public SomeClient(IService service) { Service = service; }
    }
```

Let's compare creation of `SomeClient` manually and with DryIoc.

How to instantiate `SomeClient` with [DI principle](http://en.wikipedia.org/wiki/Dependency_inversion_principle) in mind:
```
#!c#
    IClient client = new SomeClient(new SomeService());
```

That's hard-wired!

How to do that with DryIoc:
```
#!c#
    var c = new Container();
    c.Register<IClient, SomeClient>();
    c.Register<IService, SomeService>();
    
    // somewhere else
    IClient client = c.Resolve<IClient>();
```

In DryIoc we are declaring/registering mapping between interfaces and implementations.
Then __in different space and time__ we are deciding to get/resolve `IClient` object with its dependencies by providing only client interface.

Hey, it means that I can register different `IClient` implementations without touching consumer resolution code. And we are not speaking about [_good_ singletons](ReuseAndScopes) yet.

So IoC container helps me to enforce [Open-Closed principle](http://msdn.microsoft.com/en-us/magazine/cc546578.aspx)
and to improve __Testability__ and __Extensibility__ of my software.


## Usage Guide

- [Installing DryIoc](InstallationOptions)
- [Creating and Disposing Container](CreatingAndDisposingContainer)
- [Register and Resolve](RegisterResolve)
- [Open-generics](OpenGenerics)
- [Specify Constructor or Factory Method](SelectConstructorOrFactoryMethod)
- [Specify Dependency or Primitive Value](SpecifyDependencyAndPrimitiveValues)
- [Reuse and Scopes](ReuseAndScopes)
- [Wrappers](Wrappers)
- [Decorators](Decorators)
- [Error Detection and Resolution](ErrorDetectionAndResolution)
- [Rules and Default Conventions](RulesAndDefaultConventions)
- [FAQ - Migration from Autofac](FaqAutofacMigration)

- Advanced:

    - ["Child" Containers](KindsOfChildContainer)
    - [Required Service Type](RequiredServiceType)
    - [Examples of context based resolution](ExamplesContextBasedResolution)
    - [Unregister and Resolution Cache](UnregisterAndResolutionCache)
    - [Auto-mocking in Tests](UsingInTestsWithMockingLibrary)
    - [Thread-Safety](ThreadSafety)


## Companions

- [DryIocZero](Companions/DryIocZero)
- [DryIocAttributes](Companions/DryIocAttributes)

## Extensions

- [DryIoc.MefAttributedModel](Extensions/MefAttributedModel) 
for [MEF Attributed Model](http://msdn.microsoft.com/en-us/library/ee155691(v=vs.110).aspx)

- ASP.NET: 

    - [DryIoc.Web](https://www.nuget.org/packages/DryIoc.Web/) 
    - [DryIoc.Mvc](https://www.nuget.org/packages/DryIoc.WebApi.dll/)
    - [DryIoc.WepApi](https://www.nuget.org/packages/DryIoc.WebApi.dll/)
    - [DryIoc.SignalR](Extensions\SignalR)
    - [DryIoc.Dnx.DependencyInjection-RC](https://www.nuget.org/packages/DryIoc.dnx.dependencyinjection) 
for AspNetCore (vNext / DNX) 

- OWIN:

    - [DryIoc.Owin](https://www.nuget.org/packages/DryIoc.Owin.dll/)
    - [DryIoc.WebApi.Owin](https://www.nuget.org/packages/DryIoc.WebApi.Owin.dll/)

- [Nancy.Bootstrappers.DryIoc](https://www.nuget.org/packages/Nancy.Bootstrappers.DryIoc/) for [NanxyFX](http://nancyfx.org/)
- [Common Service Locator](https://www.nuget.org/packages/DryIoc.CommonServiceLocator.dll/)


## Latest Version

Get from NuGet:  `PM> Install-Package DryIoc.dll` or as code `PM> Install-Package DryIoc`

### v2.5.0 / 2016-05-24

  - fixed: #280: LazyEnumerable dependency silences missing dependency error - but works (throws) for fixed array
  - fixed: #279: IfUnresolved.ReturnDefault propagates down the dependency chain
  - fixed: #285: Recursive dependency in the case of using Composite Pattern
  - fixed: #219: Minimize memory allocations in Request and RequestInfo
  - added: #282: Enable to resolve func with more than 4 parameters (up to 7)
  - added: #255: Make GetCurrentScope available on IContainer via extension method
  - added: #172: Reuse passed Func argument in nested dependencies, but not in the same service
  - added: #257: Support for contentFiles to enable source packages in NuGet3.3
  - added: Uap (Universal Windows Platform) content source code DryIoc package


### [Previous Versions](https://bitbucket.org/dadhi/dryioc/wiki/VersionHistory)