# Error Detection and Resolution

[TOC]

## Overview

DryIoc mantras:

- Be as deterministic as possible but provide reasonable defaults.
- Try to detect errors as early as possible, better at compile-time. If impossible then detect errors on registration not later on resolution.
- __Never fail silently__ and provide information about problem, context and possible fix. 


## DryIoc exceptions

There is only one `ContainerException` that is derived from `InvalidOperationException`. It also used for checking input arguments instead of `Argument...Exception`, this way you know that container is culprit, not some other code.

`ContainerException` has two important properties: 

- `Message` display info about problem cause, context and possible fix.
- `Error` provides error-code to test and filter corresponding error case.

All DryIoc error use-cases with their `Error` and `Message` are listed in `DryIoc.Error` class. If in doubt you may look what DryIoc is capable to detect and what level of information it provides.


## Unable to resolve

Very common case when you forgot to register required service/dependency or Container was unable to use existing registrations for specific reason.

### UnableToResolveUnknownService

For instance if no registration exist for the service type - nor keyed nor default, then the error will be:
```
#!c#
    class X 
    { 
        X(Y y) {}
    }
    
    container.Register<X>(); 
    // forgot to register Y
    
    container.Resolve<X>();
    
    /* ContainerException message:
    Unable to resolve MyNamespace.Y as parameter "y"
      in MyNamespace.X.
    Where no service registrations found
      and number of Rules.FallbackContainers: 0
      and number of Rules.UnknownServiceResolvers: 0
    */
```


### UnableToResolveFromRegisteredServices

If `Y` was registered with key then DryIoc will list available registrations and provide info about current and resolution scopes:
```
#!c#
    container.Register<X>();
    container.Register<Y>(serviceKey: "special");
    
    container.Resolve<X>();
    
    /* ContainerException message:
    Unable to resolve MyNamespace.Y as parameter "y"
      in MyNamespace.X
    Where CurrentScope: null
      and ResolutionScope: null
    Found registrations:
      {"special", {ID=19, ImplType=MyNamespace.Y}}
    */
```

Information about scopes may be useful if `Y` is registered with `Reuse.InCurrentScope` but no current scope is opened at the moment `CurrentScope: null`. Or if reuse targets named scope but there is no current scope with such name.
```
#!c#
    container.Register<X>();
    container.Register<Y>(Reuse.InCurrentNameScope("specialScope"));
    
    using (container.OpenScope("luckyScope"))
        container.Resolve<X>();
    
    /* ContainerException message:
    Unable to resolve MyNamespace.Y as parameter "y"
      in MyNamespace.X
    Where CurrentScope: {Name=luckyScope, Parent=null}
      and ResolutionScope: null
    Found registrations:
      without matching scope {DefaultKey.Of(0), {ID=19, ImplType=MyNamespace.Y, Reuse=CurrentScopeReuse {Name="specialScope", Lifespan=100}}}
    */
```

`without matching scope` phrase provides the hint why resolution failed. Further comparison of Current and Reuse scope indicates name difference.


## RecursiveDependencyDetected

The problem corresponds to the cycle or recursive dependency in object graph. Better illustrated with code:
```
#!c#
    class A 
    {
        public A(B b) { } // A requires B
    }
    
    class B 
    {
       public B(A a) { } // and B requires A
    }
```

Straightforward approach of creating `A` with `new` will fail:
```
#!c#
    new A(new B(new A // Infinite loop!
``` 

Fails for Container as well.

__Note:__ Recursive dependency usually points to design problem. That's why some languages check and prohibit it at compile-time, e.g. F# - but not C#.

DryIoc will throw `ContainerException` with `Error.RecursiveDependencyDetected` when resolving either `A` or `B`:
```
#!c#
    container.Register<A>();
    container.Register<B>();
    
    container.Resolve<A>(); // throws
    
    /*
    ContainerException message:
    
    Recursive dependency is detected when resolving
    MyNamespace.A as parameter "a" <--recursive
      in MyNamespace.B as parameter "b"
      in MyNamespace.A <--recursive.
    */
```

`<--recursive` markings identify exact points in object graph when recursion is introduced.


### How to allow recursive dependency

It is up to you. It may look like the child dependency wants to access parent - __but only after parent-child relationship is already created__:
```
#!c#
    class Parent 
    {
    	public(Child child) {}
    }
    
    
    class Child 
    {
    	public(Lazy<Parent> parentAccess) { /* may only store parentAccess for future use */ }
    }
    
    
    Parent parent = null;
    var parentAccess = new Lazy<Parent>(() => parent);
    parent = new Parent(new Child(parentAccess)); 
    // Only after that point parentAccess will return initialized parent.

```

__Already hard to comprehend, right? Then please avoid it!__

DryIoc has support for `Lazy` wrapper that works as "generally" expected from IoC frameworks:
```
#!c#
    container.Register<Child>();
    container.Register<Parent>();
    
    var parent = container.Resolve<Parent>(); // works without error
```

Another way is to use `Func` wrapper But it requires introducing of new resolution root/scope. Read further ... 

DryIoc default way of injecting Func:
```
#!c#
    new Parent(new Child(
       () => new Parent // throws - Recursion in Func!
``` 

Introducing new resolution root/scope:
```
#!c#
    // setup Parent to be injected as resolution root:
    container.Register<Parent>(setup: Setup.With(openResolutionScope: true));
    
    // Parent creation will change to:
    new Parent(new Child(
        () => resolver.Resolve<Parent>())); // works - created parent will be resolved when Func called
```


## Service Registrations Diagnostics

DryIoc provides a way to examine potential errors in Container registrations __prior to actual service resolution__ via `VerifyResolutions` method. The method "resolves" all or selected registrations, catches resolution exceptions, and returns them to you for examination.

__Note:__ `VerifyRsolutions` does not actually create any service objects, neither affects container state. So you may to use container for real resolution afterwards/

```
#!c#
    // Given following SUT
    public class RequiredDependency {}
    public class MyService { public MyService(RequiredDependency d) {} }

    // Let's assume we forgot to register RequiredDependency
    var container = new Container();
    container.Register<MyService>();
    
    // Find what's missing
    var results = container.VerifyResolutions();
    Assert.AreEqual(1, results.Length);
    
```

Verify results in array of `ServiceRegistrationInfo` and `ContainerException` KeyValuePairs. In this example the result registration info will be:

> MyNamespace.MyService registered as factory {ID=14, ImplType=MyNamespace.MyService}

And the exception will be:
> DryIoc.ContainerException: Unable to resolve MyNamespace.RequiredDependency as parameter "d"
>  in MyNamespace.MyService.  
Where no service registrations found  
>  and number of Rules.FallbackContainers: 0  
>  and number of Rules.UnknownServiceResolvers: 0  

VerifyResolutions allows to filter service registrations in question by providing optional `Func<ServiceResgitrationInfo, bool> whatRegistrations` parameter. 


### VerifyResolutions is ignorant version of GenerateResolutionExpressions

Internally `VerifyResolution` uses another public method `GenerateResolutionExpressions` to get the result exceptions, and simply ignores generated  __successful resolution expressions__. 

Generated resolution expressions are expression-trees `Expression<DryIoc.FactoryDelegate>`, and may be examined, or  
__even compiled to actual delegates and used for container-less service resolution__.

__Note:__ [DryIocZero](Companions/DryIocZero) package uses the `GenerateResolutionExpressions` to generate factory delegates at compile-time.
