# Open-generics

[TOC]

## Registering open-generic service

Registering open-generic is no different from the non-generic service. The only limitation is imposed by C# itself - it is impossible to specify type statically:

    container.Register(typeof(ICommand<>), typeof(DoSomethingCommand<>));
    
    // but I cannot do:
    // container.Register<ICommand<>, DoSomethingCommand<>>();

    // Resolving:
    container.Resolve<ICommand<MyData>>();

The rest of API is identical. Here the variants:

    container.Register(typeof(Command<>));

    container.Register(typeof(Command<>), reuse: Reuse.Singleton);

    container.Register(typeof(ICommand<>), typeof(Command<>), serviceKey: "blah", ifUnresolved: IfUnresolved.ReturnDefault);

    container.Register(typeof(ICommand<>), typeof(LoggingCommand<>), setup: Setup.Decorator);

__Note:__ Registered closed-generic service has priority over corresponding open-generic:

    container.Register<My<int>, A<int>>();
    container.Register(typeof(My<>), typeof(B<>));

    container.Resolve<My<int>>() // will return A<int> istead of B<int>

    // But resolving as collection will return both:
    container.Resolve<My<int>[]>(); // contains A<int> and B<int>


## Matching type arguments constraints

DryIoc will evaluate type argument constraints when resolving open-generic. Let's review specific cases where it may be useful

### Filter services in collection based on constraints

Example:
```
#!c#
    public interface I<T> { }
    public A<T> : I<T> where T : IDisposable { }
    public B<T> : I<T> { }
    
    container.RegisterMany(new[] { typeof(A<>), typeof(B<>) });
    
    var items = container.Resolve<IEnumerable<I<string>>>();
    
    // The only result item will be of type B<> 
    // An A<> was filtered out because string is matching to IDisposable constraint.
    Assert.IsInstanceOf<B<string>>(items.Single()); 
```


### Fill-in type arguments from constraints

Example:

```
#!c#
    public interface ICommandHandler<TCommand> { }
    public class SpecialEntity { }
    public class UpdateCommand<TEntity> { }
    
    public class UpdateCommandHandler<TEntity, TCommand> : ICommandHandler<TCommand>
        where TEntity : SpecialEntity
        where TCommand : UpdateCommand<TEntity> { }
    
    public class MyEntity : SpecialEntity { }
    
    [Test]
    public void Can_fill_in_type_argument_from_constraint()
    {
        var container = new Container();
        container.Register(typeof(ICommandHandler<>), typeof(UpdateCommandHandler<,>));
    
        var handler = container.Resolve<ICommandHandler<UpdateCommand<MyEntity>>>();
    
        Assert.IsInstanceOf<UpdateCommandHandler<MyEntity, UpdateCommand<MyEntity>>>(handler);
    }
```

In this example DryIoc is smart enough to use `MyEntity` as `UpdateCommandHandler` first type argument, given rules provided by constraints.

__Note:__ This example is not so uncommon in a modern world, e.g. [MediatR](https://github.com/jbogard/MediatR).


## Generic variance when resolving many services

When resolving many or collection of generic types DryIoc will include variance-compatible types. By example:

    public interface IHandler<in TEvent> {} // contravariant handler

    public class A {}
    public class B : A {}

    public class AHandler : IHandler<A> {}
    public class BHandler : IHandler<B> {}

    // register handlers
    container.Register<IHandler<A>, AHandler>();
    container.Register<IHandler<B>, BHandler>();

    // get all handlers of A
    var ahandlers = container.ResolveMany<IHandler<A>>();

    // Result contains both AHandler and BHandler, 
    // because IHandler<B> is assignable to IHandler<A> due variance rules
    Assert.AreEqual(2, ahandlers.Count());

    // Result contains only BHandler
    var bhandlers = container.ResolveMany<IHandler<B>>();
    Assert.AreEqual(1, bhandlers.Count());

This rule is enabled by default. To turn it off:

    var container = new Container(rules =>
        rules.WithoutVariantGenericTypesInResolvedCollection());

    // the same setup ...
    // but result contains AHandler only
    var ahandlers = container.ResolveMany<IHandler<A>>();
    Assert.AreEqual(1, ahandlers.Count());