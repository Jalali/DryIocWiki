Version History
---------------

### v2.4.3 / 2016-05-08

  - fixed: #277 Custom value for dependency evaluated to null is interpreted as no custom value and tries to resolve the dependency
  - fixed: #278 Arg.Of does not recognize service key of non-primitive type

### v2.4.2 / 2016-04-26 

  - fixed: #274: Lazy resolution of dependency registered with Reuse.InResolutionScope breaks subsequent resolutions without wrappers

### v2.4.1 / 2016-04-16

  - fixed: #267: False alarm about recursive dependency when using decorator

### v2.4.0 / 2016-04-14

  - added: #263: Add IfAlreadyRegistered.AppendNewImplementation option for better collection handling
  - fixed: #264: IfAlreadyRegistered.Replace can span multiple registrations
  - fixed: #262: Using attributes to inject primitive variables
  - fixed: #261: Make Disposal work in reverse resolution order

### v2.3.0 / 2016-03-30

  - fixed: #260: Cannot interpret GetOrCreateResolutionScope expression on iOS
  - fixed: #258: Setup.AllowDisposableTransient is overridden by container.WithTrackingDisposableTransient
  - changed: In source code DryIoc package split immutable data structures and tool to separate ImTools.cs
  - removed: CloseCurrentScope from Container as it was added recently but not used anywhere.
  - added: method New overload with strongly type Made.Of

### v2.2.2 / 2016-03-10

  - fixed: #251: Auto register types from different namespace and different assemblies
  - fixed: #252: Make work together MefAttributedModel constructor selection and ConstructorWithResolvableArguments rule
  - fixed: #253 Add Container.ToString method to at least indicate scope for scoped container

### v2.2.1 / 2016-03-08

  - fixed: #245: Automatically add condition to check T constraints in Decorator of T
  - fixed: #250: DI.Mef: Non static class of static factory method is exported and can be resolved
  - changed: Minor Resolve speedup

### v2.2.0 / 2016-02-26

  - added: #141: Support Decorators with open-generic factory methods of T
  - added: #206: Track IDisposable Transients in scoped consumer's Scope
  - added: #215: Option to specify Release action for reused services to implement pooling etc
  - added: #227: Missing Arg.Of overload to specify default value for unresolved dependency
  - added: #228: Make IContainer implicitly available for injection without need for registration
  - added: #229: Container rule to use Singleton scope as implicitly Open current scope for scoped registrations
  - added: #239: Decorator setting to use Decoratee Reuse
  - added: #241: Registration option to useParentReuse for dependency
  - added: #242: Container rule for automatic concrete types resolution
  - fixed: #94: Support for creating concrete type without storing it in Container
  - fixed: #220: Minimize locking and therefore chances for deadlock in Singleton Scope
  - fixed: #230: Custom initializer attached to lazily resolved dependency is called once per resolution, not once per construction
  - fixed: #240: ConstructorWithAllResolvableArguments ignores implicitly injected dependencies and custom values

### v2.1.3 / 2016-01-16

  - fixed: #224: Enumerable wrapped in Func loses the information about Func wrapper, causing incorrect scope lifetime validation

### v2.1.2 / 2016-01-15

  - fixed: #222: Resolving as IEnumerable silence the dependency resolution errors and just skips the unresolved item
  - fixed: #218: Apply decorators registered for service type when resolving with required service type

### v2.1.1 / 2016-01-04

  - fixed: #213: Lazy Singletons should be resolved after container is disposed
  - fixed: #212: ResolveMany of object with generic required service type is failing with ArgumentException

### v2.1.0 / 2015-12-04

  - added: #205: Add customizable IfAlreadyRegistered default value per container
  - added: #204: Add ResolveMany of objects wout need to specify ResolveMany{Object}
  - fixed: #203: RegisterMany should exclude ValueType and general purpose service types IEquatable, IComparable
  - Small performance improvements

### v2.0.2 / 2015-12-01

  - fixed: #201: Mutithreading issue when RegisterInstance() is used within OpenScope()

### v2.0.1 / 2015-11-27

- fixed: #200: Multiple instances for Singleton created when Container is shared among multiple threads

### v2.0 / 2015-11-19

This release pushes DryIoc towards the mature [full-featured](http://featuretests.apphb.com/DependencyInjection.html) library with:

- Support for [PCL](http://msdn.microsoft.com/en-us/library/gg597391(v=vs.110).aspx) and [.NET Core](https://oren.codes/2015/07/29/targeting-net-core).
- [Documentation](Home#markdown-header-usage-guide).
- More complete and consistent API surface.
- Bug fixes.
- Diagnostics for potential resolution problems with `container.VerifyResolutions()`.
- Improved registration and first resolution time.
- Support of really large object graphs.
- Possibility of compile-time factory delegate generation (utilized by [DryIocZero](Companions/DryIocZero)).
- Ambient current scope and `Reuse.InWebRequest` for ASP.NET integration.
- Reuse per service in resolution graph via `Reuse.InResolutionScopeOf()`.
- Support for static and instance factory methods in addition to constructor, including support for method parameters injection.
- Powerful open-generics support including variance, constraints, open-generic factory methods in open-generic classes.
- Batch registration from assemblies and type collections via `RegisterMany`.
- Support for service key of arbitrary type. The only requirement for key type is to implement `object.GetHashCode` and `object.Equals`.
- Resolve as `KeyValuePair` to get service key with service object.
- Register with condition for resolution.
- Required service type support: e.g. `var serviceObjects = c.Resolve<object[]>(typeof(Service));`.
- Optional parameters support.
- Fine-grained control over injection of parameters, properties, and fields.
- Injection of primitive values.
- Control how reused service is stored and disposed via `weaklyReferenced` and `preventDisposal` setups.
- Resolve service collection as `IList<T>`, `ICollection<T>`, `IReadOnlyList<T>`, `IReadOnlyCollection<T>`.  
- Register once, existing registration update, unregister.
- __removed:__ Compilation to DynamicAssembly. DryIoc is fast enough without its complexity. 

### v1.4.1 / 2014-10-09

- fixed: #70: .Net 4.0 related: Unable to resolve types from unsigned assembly if compiling to dynamic assembly is turned On (it is by default)

### v1.4.0 / 2014-09-10

- fixed: #56: ResolvePropertyAndFields/SatisfyImports for already created instance with regard of ResolutionRules, e.g. provided by MefAttributedModel

### v1.3.1 / 2014-08-18

- fixed: #46: Reuse.InCurrentScope for nested dependencies is not working

### v1.3.0 / 2014-07-16

- fixed: #5: Friend assembly reference 'DryIoC.CompiledFactoryProvider.DynamicAssembly' is invalid. Strong-name signed assemblies must specify a public key in their InternalsVisibleTo declarations.
- fixed: #4: DryIoC\InternalsVisibleToFactoryCompilerDynamic.cs.
- fixed: #37: Support symbol packages for SymbolSource.org.
- fixed: #38: Add optional script to Sign NuGet package assemblies with Strong Name

### v1.2.2 / 2014-07-02

- fixed: #29: DryIoc.dll v1.2.1 Nuget package contains wrong DLL from dev branch

### v1.2.1 / 2014-06-30

- fixed: #26: Singleton creation from new scope fails

### v1.2.0 / 2014-01-09

- fixed: #1: Reordering and nesting of type arguments in generics is not supported

### v1.1.1 / 2013-12-26

- DryIoc first public appearance.