# Installation Options

[TOC]

## Source and Binary NuGet packages


DryIoc and its extensions distributed via [NuGet](https://www.nuget.org/packages?q=dryioc) either as Source or Binary package.

- Source package contains source files: _Container.cs_ and couple of other depending on target framework. All files will be located in target project under _DryIoc_ folder.  
Installing Source package V2.0rc: `PM> Install-Package DryIoc -Pre`

- Binary package names are suffixed with __".dll"__ and contain DLLs that normally referenced by target project.  
Installing Binary package V2.0rc: `PM> Install-Package DryIoc.dll -Pre`

__Note:__ Binary package in .NET 4.0 and higher will add _InternalsVisibleToDryIocFactoryCompilerDynamicAssembly.cs_ source file to target project Properties folder. This file is optional and may removed be if you are not using `Rules.WithFactoryDelegateCompilationToDynamicAssembly` rule. This rule is switched off by default in V2.0.


## Assembly Signing

By default DryIoc binaries provided without strong-signing. For those who requires strong-signing DryIoc package includes two files:

- _tools\SignPackageAssemblies.ps1_
- _tools\DryIoc.snk_

Run _SignPackageAssemblies.ps1_ from __NuGet Package Manager -> Package Manager Console__ to sign installed DryIoc assemblies with _DryIoc.snk_ key.

__Note:__ Given you made assemblies strongly-signed and if you are using `Rules.WithFactoryDelegateCompilationToDynamicAssembly` please go to target project _Properties\InternalsVisibleToDryIocFactoryCompilerDynamicAssembly.cs_ and uncomment corresponding lines as stated inside the file.