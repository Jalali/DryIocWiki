Using in Tests with Mocking library
-----------------------------------

### Auto-mocking
We can setup container to return mocks for unregistred services. In example below we will use [NSubstitute](http://nsubstitute.github.io/) library for creating mocks, But you can use something else.

Given setup with not implemented interface as below:

    public interface INotImplementedService { }

    public class TestClient
    {
        public INotImplementedService Service { get; set; }

        public TestClient(INotImplementedService service)
        {
            Service = service;
        }
    }


Let's test TestClient by mocking its Service dependency:

    var container = new Container(rules => rules.WithUnknownServiceResolvers(request =>
    {
        var serviceType = request.ServiceType;
        if (!serviceType.IsAbstract)
            return null; // Mock interface or abstract class only.

        return new ReflectionFactory(made: Made.Of(
            () => Substitute.For(Arg.Index<Type[]>(0), Arg.Index<object[]>(1)),
            _ => new[] { serviceType }, _ => (object[])null));
    }));

    var sub = container.Resolve<INotImplementedService>();

    Assert.That(sub, Is.InstanceOf<INotImplementedService>());

__Note:__ If you want to __not mock__ but automatically create concrete classes you can use `container.WithAutoFallbackResolution` method described [here](UnknownServiceResolvers#markdown-header-unknownserviceresolvers).
