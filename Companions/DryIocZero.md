# DryIocZero

[TOC]

## At Glance

NuGet: `PM> Install-Package DryIocZero`

Standalone container based on compile-time generated factories by DryIoc. 

- __Works standalone without any run-time dependencies.__
- Ensures _zero_ application bootstrapping time associated with IoC registrations.
- Provides verification of DryIoc registrations at compile-time by generating service factory delegates. 
Basically you can see how DryIoc is creating things.
- Supports everything registered in DryIoc: reuses, decorators, wrappers, etc.
- Much smaller and simpler than DryIoc itself. 
- Additionally supports run-time registrations: you may register instances and factories at run-time.