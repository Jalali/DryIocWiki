# Decorators

[TOC]

## Overview

Decorator in DryIoc generally represents 
[Decorator Pattern](https://en.wikipedia.org/wiki/Decorator_pattern).
But in conjunction with other DryIoc features, especially with [Factory Method](SelectConstructorOrFactoryMethod#markdown-header-factory-method-instead-of-constructor) 
the decorator concept may be extended further to cover more interesting scenarios.

DryIoc Decorators support:

- General decorating of service with adding functionality around decorated service calls.
- Applying decorator based on condition.
- Nesting decorators and specifying custom nesting order.
- Open-generic decorators with support of: generic variance, constraints, generic factory methods in generic classes.
- Decorator may have its own Reuse different from decorated service. There is an additional option for decorators to `useDecorateeReuse`.
- Decorator may decorate service wrapped in `Func`, `Lazy` and the rest of [Wrappers](Wrappers). Nesting is supported as well.
- Decorator registered with Factory Method may be used to add functionality around decorated service creation, 
aka __Initializer__.
- Combining decorator reuse and factory method registration, decorator may provide additional action
on service dispose, aka __Disposer__.
- Using Factory Method and decorator condition it is possible to register decorator of generic `T` service type. 
This opens possibility for more generic use-cases, e.g. EventAggregator with attributed subscribe.
- Combining Decorator with library like __Castle.DynamicProxy__ enables 
Interception and AOP functionality support.


## General use-case

Let's define logging decorator: 
```
#!c#
    public interface IHandler { void Handle(); }
    public class FooHandler : IHandler 
    {
        public void Handle() 
        { 
            IsHandled = true; 
        }
    }

    public class LoggingHandlerDecorator : IHandler 
    {
        public LoggingHandlerDecorator(IHandler handler, ILogger logger) { /* stores both */ }

        public void Handle() 
        { 
            _logger.LogInfo("About to handle ...")
            _handler.Handle(); 
            _logger.LogInfo("Successfully handled.")
        }
    }

    // easy to register
    container.Register<ILogger, MyFineLogger>();
    container.Register<IHandler, FooHandler>();
    container.Register<IHandler, LoggingHandlerDecorator>(setup: Setup.Decorator);

    // resolve as usual
    container.Resolve<IHandler>(); // returns Logging decorator with wrapped FooHandler.
```

The decorator registration is only different in `setup: Setup.Decorator` parameter. 
That means the rest of registration options are available for decorators.

__Note:__ Except `serviceKey` which is not supported. To apply decorator for service registered with `serviceKey` you can use setup condition.

```
#!c#
    container.Register<IHandler, FooHandler>(serviceKey: "fast");
    container.Register<IHandler, LoggingHandlerDecorator>(
            setup: Setup.DecoratorWith(condition: request => "fast".Equals(request.ServiceKey)));
```

## Nested Decorators

DryIoc supports decorator nesting, 
with the first registered decorator to wrap the actual service and the subsequent to wrap the already decorated objects.

```
#!c#
class A {}
class D1 : A { public D1(A a) {} }
class D2 : A { public D2(A a) {} }

container.Register<A>();
container.Register<A, D1>(setup: Setup.Decorator);
container.Register<A, D2>(setup: Setup.Decorator);

var a = container.Resolve<A>();

// a is created as `new D2(new D1(new A()))`
// the type of a is the last registered decorator
Assert.IsInstanceOf<D2>(a);
```

### Decorators Order

The order of decorator nesting may be explicitly specified with `Order` setup option:
```
#!c#
container.Register<A>();
container.Register<A, D1>(setup: Setup.DecoratorWith(Order: 2));
container.Register<A, D2>(setup: Setup.DecoratorWith(Order: 1));

var a = container.Resolve<A>();

// a is created as `new D1(new D2(new A()))`
Assert.IsInstanceOf<D1>(a);
```

The decorators without defined order have implicit `Order` of _0_. Decorators with identical `Order` are ordered by registration.
You can specify _-1_, _-2_, etc. order to insert decorator closer to decorated service.

__Note:__ The order does not prefer specific decorator type: concrete, open-generic and decorators of generic `T` 
are ordered based on the same rules.


## Open-generic decorators

Decorators may be open-generic and registered to wrap open-generic services. 
They could be nested as well.
```
#!c#
    // constructors are skipped for brevity, they just accept A parameter
    class A<T> {}
    class D1<T> : A<T> {}
    class D2<T> : A<T> {}
    class Ds : A<string> {} 

    container.Register(typeof(A<>));
    container.Register(typeof(A<>), typeof(D1<>), setup: Setup.Decorator);
    container.Register<A<string>, Ds>(setup: Setup.Decorator);
    container.Register(typeof(A<>), typeof(D2<>), setup: Setup.Decorator);

    var aint = container.Resolve<A<int>>();
    // the aint will be created as: new D2<int>(new D1<int>(new A<int>()));
    // Ds decorator is not apppied because it is defined only for A<string>

    var astr = container.Resolve<A<int>>();
    // the astr will be created as: new D2<string>(new Ds<string>(new D1<string>(new A<string>())));
    // Ds is applied as expected
```

## Decorator of generic T

Decorator may be registered as [FactoryMethod](SelectConstructorOrFactoryMethod#markdown-header-factory-method-instead-of-constructor)
which brings interesting question what if method is generic and returns generic parameter `T`:
```
#!c#
    static class DecoratorFactory 
    {
        public static T Decorate<T>(T service) where T : IStartable
        {
            service.Start();
            return service;
        }
    }
```

Given this example it will be interesting if we can use `Decorate` method to register decorator applied to any `T` service.

Starting from v2.2 DryIoc supports this scenario by registering decorator of `object`:
```
#!c#
    public class X : IStartable
    {
        public bool IsStarted { get; private set; }

        public void Start()
        {
            IsStarted = true;
        }
    }

    container.Register<X>();
    container.Register<object>(
        made: Made.Of(r => typeof(DecoratorFactory)
            .GetSingleMethodOrNull("Decorate")
            .MakeGenericMethod(r.ServiceType)), 
        setup: Setup.Decorator);

    var x = container.Resolve<X>();
    Assert.IsTrue(x.IsStarted);
```

The major _problem_ of `object` decorators that they will be applied to all services,
affecting the resolution performance. To make decorator more targeted we can provide 
the condition parameter.

We can add condition to apply decorator only to `IStartable` services:

```
#!c#
    container.Register<object>(
        made: Made.Of(r => typeof(DecoratorFactory).GetSingleMethodOrNull("Decorate")
            .MakeGenericMethod(r.ServiceType)), 
        setup: Setup.DecoratorWith(
            condition: r => (r.ImplementationType ?? r.ServiceType).IsAssignableTo(typeof(IStartable))));
```

## Decorator Reuse

Decorator may have their own reuse the same way as normal services. 
This way you may add context based reuse to service just by applying the decorator.

__Note:__ If no reuse specified for decorator, it means that decorator is Transient.
It may be not what you want especially for decorating reused service - for this case
DryIoc supports `useDecorateeReuse` [setup option](Decorators#markdown-header-usedecorateereuse).

```
#!c#

container.Register<A>();
container.Register<A, D>(Reuse.Singleton, setup: Setup.Decorator);

var a1 = container.Resolve<A>();
var a2 = container.Resolve<A>();
Assert.AreSame(a1, a2);
```


### UseDecorateeReuse

This setup option allows decorator to follow decorated service reuse. 
It is good option when you don't want to adjust decorated service reuse 
but want to add some independent functionality, like logging. 

Decorated service reuse may be changed without need to adjust decorator registrations. 
```
#!c#

container.Register<A>(Reuse.Singleton);
container.Register<A, D>(setup: Setup.DecoratorWith(useDecorateeReuse: true));

var a1 = container.Resolve<A>();
var a2 = container.Resolve<A>();
Assert.AreSame(a1, a2); // because of decorated service reuse.
```


## Decorated Wrappers

Decorator may be applied to [wrapped service](Wrappers) in order to provide laziness, create decorated service on demand, 
proxying, etc.
```
#!c#
    class A { virtual void Call() {} }
    class LazyADecorator : A
    { 
        public Lazy<A> _a;
        public LazyADecorator(Lazy<A> a) { _a = a; }
     
        public override void Call() 
        {
            // gets A value lazily on demand:
            _a.Value.Call();
        }
    }

    container.Register<A>();
    container.Register<A, LazyADecorator>();

    var a = container.Resolve<A>(); // A is not created.
    a.Call(); // A is created and called.
```

Decorators of wrappers may be nested the same way as for normal services:
```
#!c#
    public class AsLazy : A
    {
        public AsLazy(Lazy<A> a) { A = a; }
    }

    public class AsFunc : A
    {
        public AsLazy(Func<A> a) { A = a; }
    }

    container.Register<A>();
    container.Register<AsLazy, A>(setup: Setup.Decorator);
    container.Register<AsFunc, A>(setup: Setup.Decorator);

    var a = container.Resolve<A>() // wrapped in AsFunc
    a = ((AsFunc)a).A();           // wrapped in AsLazy
    a = ((AsLazy)a).A.Value;       // actual decorated A
```


## Decorator as Initializer

When registering decorator with [Factory Method](SelectConstructorOrFactoryMethod#markdown-header-factory-method-instead-of-constructor)
it is possible to __decorate the creation of service__. 

It means that decorator factory method accepts injected decorated service, 
invokes some initialization logic on the decorated instance, 
and returns this instance. Decorator may or may not replace the instance completely.
```
#!c#
    public class Foo : IFoo 
    {
        public string Message { get; set; }
    }

    public static class FooMiddleware 
    {
        public static IFoo Greet(IFoo foo) 
        {
            foo.Message = "Hello, " + (foo.Message ?? "");
            return foo;
        }
    }

    container.Register<IFoo, Foo>();
    container.Register<IFoo>(
        Made.Of(() => FooMiddleware.Greet(Arg.Of<IFoo>())),
        setup: Setup.Decorator)

    var foo = container.Resolve<IFoo>();
    StringAssert.Contains("Hello", foo.Message);
```

Here `FooMiddleware.Greet` is static method just for demonstration. 
It may be non-static and include additional dependencies to be injected by Container. 
Check [Factory Method docs](SelectConstructorOrFactoryMethod#markdown-header-factory-method-instead-of-constructor) for more details.

DryIoc has a dedicated `RegisterInitializer` [method](RegisterResolve#markdown-header-registerinitializer) which is actual decorator in disguise.
Here is the implementation of register initializer for illustration of the idea:
```
#!c#

    // Step 1:
    // Define the decorator factory which accepts user initialize action.
    // The actual decorator method is Decorate which just invokes stored action and 
    // returns decorated service instance.
    internal sealed class InitializerFactory<TTarget>
    {
        private readonly Action<TTarget, IResolver> _initialize;

        public InitializerFactory(Action<TTarget, IResolver> initialize)
        {
            _initialize = initialize;
        }

        public TService Decorate<TService>(TService service, IResolver resolver)
            where TService : TTarget
        {
            _initialize(service, resolver);
            return service;
        }
    }

    // Step 2: Register the decorator factory and decorator produced by Decorate method.

    // unique key to bind decorator factory and decorator registrations
    var decoratorFactoryKey = new object();

    // register decorator factory and identify it with the key
    registrator.RegisterDelegate(
        _ => new InitializerFactory<TTarget>(initialize),
        serviceKey: decoratorFactoryKey);

    // decorator condition to make decorator applied only to TTarget service type.
    Func<RequestInfo, bool> decoratorCondition = r => 
        (r.ImplementationType ?? r.GetActualServiceType()).IsAssignableTo(typeof(TTarget));

    // register decorator with Decorate method of factory identified with the key
    registrator.Register<object>(
        made: Made.Of(request => FactoryMethod.Of(
            typeof(InitializerFactory<TTarget>).GetSingleMethodOrNull("Decorate").MakeGenericMethod(request.ServiceType),
            ServiceInfo.Of<InitializerFactory<TTarget>>(serviceKey: decoratorFactoryKey))),
        setup: Setup.DecoratorWith(decoratorCondition, useDecorateeReuse: true));
```

__The example illustrates:__ use of decorator of generic type `T`, use of instance factory method,
and `useDecorateeReuse`.


## Decorator as Disposer

DryIoc supports [custom Dispose action](RegisterResolve#markdown-header-registerdisposer). 
As you may predict the disposer is also implemented as decorator:

    // Step 1:
    // Define decorator factory to store dispose action 
    // and actual decorator method "TrackForDispose"
    internal sealed class Disposer<T> : IDisposable
    {
        private readonly Action<T> _dispose;
        private int _state;
        private const int Tracked = 1, Disposed = 2;
        private T _item;

        public Disposer(Action<T> dispose)
        {
            _dispose = dispose.ThrowIfNull();
        }

        public T TrackForDispose(T item)
        {
            if (Interlocked.CompareExchange(ref _state, Tracked, 0) != 0)
                Throw.It(Error.Of("Something is {0} already."), _state == Tracked ? " tracked" : "disposed");
            _item = item;
            return item;
        }

        public void Dispose()
        {
            if (Interlocked.CompareExchange(ref _state, Disposed, Tracked) != Tracked)
                return;
            var item = _item;
            if (item != null)
            {
                _dispose(item);
                _item = default(T);
            }
        }
    }

    // Step 2:
    // Register factory and decorator via factory method
    var disposerKey = new object();

    registrator.RegisterDelegate(_ => new Disposer<TService>(dispose), 
        serviceKey: disposerKey,
        setup: Setup.With(useParentReuse: true));

    registrator.Register(Made.Of(
        r => ServiceInfo.Of<Disposer<TService>>(serviceKey: disposerKey),
        f => f.TrackForDispose(Arg.Of<TService>())),
        setup: Setup.DecoratorWith(condition, useDecorateeReuse: true));

__The example illustrates:__ use of instance factory method for registering decorator, `useDecorateeReuse`, `useParentReuse` for a factory.


## Decorator as Interceptor with Castle DynamicProxy

Decorator pattern is a good fit for implementing [cross-cutting concerns](https://en.wikipedia.org/wiki/Cross-cutting_concern). 
We can extend it further to implement [AOP Interception](https://en.wikipedia.org/wiki/Aspect-oriented_programming) 
with help of [Castle DynamicProxy](http://www.castleproject.org/projects/dynamicproxy/).

Let's define little extension method for intercepting interfaces:

    using Castle.DynamicProxy;

    public static class DryIocInterceptionTools
    {
        public static void RegisterInterfaceInterceptor<TService, TInterceptor>(this IRegistrator registrator)
            where TInterceptor : class, IInterceptor
        {
            var serviceType = typeof(TService);
            if (!serviceType.IsInterface)
                throw new ArgumentException(string.Format("Intercepted service type {0} is not an interface", serviceType));

            // Create proxy type for intercepted interface
            var proxyType = ProxyBuilder.Value.CreateInterfaceProxyTypeWithTargetInterface(
                serviceType, new Type[0], ProxyGenerationOptions.Default);

            // Register proxy as decorator
            registrator.Register(serviceType, proxyType,
                made: Parameters.Of.Type<IInterceptor[]>(typeof(TInterceptor[])),
                setup: Setup.Decorator);
        }

        private static readonly Lazy<DefaultProxyBuilder> ProxyBuilder = new Lazy<DefaultProxyBuilder>(() => new DefaultProxyBuilder());
    }

Then we can register an interceptor:

    public class FooLoggingInterceptor : IInterceptor 
    {
        public void Intercept(IInvocation invocation)
        {
            Logger.Log("Invoking: {0}", invocation.GetConcreteMethod().Name);
            invocation.Proceed();
        }
    }

    container.Register<IFoo, Foo>();
    container.Register<FooLoggingInterceptor>();
    container.RegisterInterfaceInterceptor<IFoo, FooLoggingInterceptor>();