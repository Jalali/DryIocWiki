# Creating and Disposing Container

[TOC]

## Creating Container

In most cases all you need is: `new Container();`. 

Actually this statement is equivalent to: 

```
#!c#
var container = new Container(
	rules: Rules.Default, 
	scopeContext: new AsyncExecutionFlowScopeContext());
```

__Note:__ `AsyncExecutionFlowScopeContext` is default for __.NET 4.5 and higher__, for lower or PCL .NET targets the default is `ThreadScopeContext`.

You may specify the rules or [scopeContext](https://bitbucket.org/dadhi/dryioc/wiki/ReuseAndScopes#markdown-header-what-scopecontext-is) if you want.


### Rules

Rules define container behavior and conventions. Rules configuration is immutable and provide `With/Without` methods to produce new rules with specific settings turned On/Off.

Rules examples:
```
#!c#
    var container1 = new Container(  
        Rules.Default.With(FactoryMethod.ConstructorWithResolvableArguments));
    
    var container2 = new Container(
        Rules.Default.WithUnknownServiceResolver(Container.FallbackToContainers(container1)));
    
    var container3 = new Container(
        Rules.Default.WithoutThrowIfDependencyHasShorterReuseLifespan());
```

There is alternate way to specify rules without knowing about container default rules:
```
#!c#
    var container3 = new Container(
       rules => rules.WithoutThrowIfDependencyHasShorterReuseLifespan());
```

Given whatever are current container rules specify how to change them.


### ScopeContext

ScopeContext is described in details [here](https://bitbucket.org/dadhi/dryioc/wiki/ReuseAndScopes#markdown-header-what-scopecontext-is).


## Disposing Container

`Container` class implements `IDisposable` interface and should be disposed when no longer required. When disposed container will do the following:

- Sets Rules to Empty.
- Removes all registrations.
- Disposes resolved Singletons.

Usage of disposed Container will end-up with corresponding exception.
